class PagesController < ApplicationController

  def index
    @index = true
    @partners = Partner.all
  end

  def blog
    lappis_blog_url = 'https://fga.unb.br/api/v1/articles/1590/children'
    @articles = JSON.load(open lappis_blog_url)["articles"].tap(&:pop)
    @articles.each { |article| article["created_at"] = DateTime.parse(article["created_at"]) }
    @articles.shift
  end

  def article
    article_url = 'https://fga.unb.br/api/v1/articles/' + params[:id]
    @article = JSON.load(open article_url)["article"]
    @article["created_at"] = DateTime.parse(@article["created_at"])
    @article_url = request.url
  end
end
