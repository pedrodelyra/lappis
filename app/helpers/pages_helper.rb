module PagesHelper
  def months_names
    ["janeiro", "fevereiro", "março", "abril",
     "maio", "junho", "julho", "agosto",
     "setembro", "outubro", "novembro", "dezembro"]
  end
end
