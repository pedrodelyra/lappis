module ApplicationHelper
  def navbar_links *desired_links
    links = { blog: content_tag(:li, link_to(font_awesome('newspaper-o') + ' Blog', '/blog')),
              about: content_tag(:li, link_to(font_awesome('users') + ' Sobre', '/#about-us', class: 'about-us')),
              products: content_tag(:li, link_to(font_awesome('line-chart') + ' Produtos', '/#products', class: 'products')),
              partners: content_tag(:li, link_to(font_awesome('heart') + ' Parceiros', '/#partners', class: 'partners')),
              search: content_tag(:li, link_to(font_awesome('search'), '#', class: 'search')),
              menu: content_tag(:li, link_to(font_awesome('bars'), '#', class: 'menu'))}
    links.slice(*desired_links).values.join { |html| "#{html}" }
  end

  def font_awesome type
    content_tag(:i, nil, class: "fa fa-#{type}")
  end

  def facebook_comments_plugin
    "https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9&appId=1185185261540232"
  end

  def footer_note
    "Este trabalho é licenciado sob Creative Commons Attribution-No Derivative Works 3.0 (ou uma versão superior)."
  end

  def lappis_intro_message
    "O LAPPIS está desenvolvendo um blog onde seus membros irão compartilhar muitas
     experiências em engenharia de software."
  end

  def about_us_text
    ["O Laboratório Avançado de Produção, Pesquisa e Inovação em Software (LAPPIS), criado em 2012 e
      localizado na Universidade de Brasília (UnB), foi projetado para atuar em diversas áreas tecnológicas,
      desde sistemas de informação até sistemas embarcados, objetivando as oportunidades de pesquisas teóricas e aplicadas.",
     "O LAPPIS possui como principal objetivo contribuir para o desenvolvimento de projetos de software, ao passo
      que complementa a formação de engenheiros de software (capazes de lidar com problemas ao pensar em soluções
      computacionais e implementá-las efetivamente), através de métodos ágeis, software livre e trabalho colaborativo
      centrado em pessoas.",
     "Com isso, oferece aos alunos de graduação em Engenharia de Software a oportunidade de aplicar conceitos e
      tecnologias em um ambiente real de produção de software, sob a orientação de professores especialistas nas
      áreas dos projetos desenvolvidos."]
  end

  def products_description
    "O LAPPIS desenvolveu vários produtos ao longo dos anos. Cada um deles teve um significado especial e trouxe grandes
     desafios a serem resolvidos que contribuíram para o crescimento de seus membros. Aqui estão alguns deles:"
  end

  def partners_text
  ["O LAPPIS contou com o apoio de vários colaboradores ao longo de sua história. Alguns seguiram seu próprio caminho
    enquanto outros continuam a desenvolver os projetos e desafios que surgem no laboratório. De qualquer forma, todos
    eles ajudaram a construir uma bela história que corrobora o fato de que é possível criar um ambiente de trabalho
    baseado na colaboração, confiança e auto-gestão e ainda ser <strong>extremamente competitivo</strong>.",
   "Aqui estão os heróis:"]
  end
end
