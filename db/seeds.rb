Partner.delete_all

Partner.create([
  {
    name:     'Paulo Meirelles',
    photo:    'prmm.jpg',
    github:   'https://github.com/prmm',
    gitlab:   'https://gitlab.com/prmm',
    linkedin: 'https://linkedin.com/in/paulormm'
  },

  {
    name:     'Hilmer Rodrigues',
    photo:    'hilmer.jpg',
    github:   '#',
    gitlab:   '#',
    linkedin: '#'
  },

  {
    name:     'Fábio Mendes',
    photo:    'fabio.jpg',
    github:   '#',
    gitlab:   '#',
    linkedin: '#'
  },

  {
    name:     'Carla Rocha',
    photo:    'carla.jpg',
    github:   '#',
    gitlab:   '#',
    linkedin: '#'
  },

  {
    name:     'Edson Alves',
    photo:    'edson.jpg',
    github:   '#',
    gitlab:   '#',
    linkedin: '#'
  },

  {
    name:     'Rodrigo Siqueira',
    photo:    'siqueira.jpg',
    github:   '#',
    gitlab:   '#',
    linkedin: '#'
  },

  {
    name:     'Tallys Martins',
    photo:    'tallys.jpg',
    github:   '#',
    gitlab:   '#',
    linkedin: '#'
  },

  {
    name:     'Arthur Del Esposte',
    photo:    'arthur-del-esposte.jpg',
    github:   'https://github.com/arthurmde',
    gitlab:   'https://gitlab.com/arthurmde',
    linkedin: 'https://www.linkedin.com/in/arthur-del-esposte-97126689/'
  },

  {
    name:     'Dylan Guedes',
    photo:    'dylan.jpg',
    github:   'https://github.com/DylanGuedes',
    gitlab:   'https://gitlab.com/DGuedes',
    linkedin: '#'
  },

  {
    name:     'Lucas Mattioli',
    photo:    'mattioli.jpg',
    github:   'https://github.com/Mattioli',
    gitlab:   'https://gitlab.com/Mattioli',
    linkedin: '#'
  },

  {
    name:     'Charles Oliveira',
    photo:    'charles.jpg',
    github:   'https://github.com/chaws',
    gitlab:   'https://gitlab.com/chaws',
    linkedin: 'https://www.linkedin.com/in/charles-oliveira-55940289/'
  },

  {
    name:     'Simião Carvalho',
    photo:    'simiao.jpg',
    github:   'https://github.com/simiaosimis',
    gitlab:   'https://gitlab.com/simiaosimis',
    linkedin: 'https://www.linkedin.com/in/simião-carvalho-b8856b113'
  },

  {
    name:     'Pedro de Lyra',
    photo:    'pedro.jpg',
    github:   'https://github.com/pedrodelyra',
    gitlab:   'https://gitlab.com/pedrodelyra',
    linkedin: 'https://www.linkedin.com/in/pedro-de-lyra-pereira-10850113b/'
  },

  {
    name:     'Victor Matias',
    photo:    'victor-matias.jpg',
    github:   'https://github.com/matias310396',
    gitlab:   'https://gitlab.com/matias310396/',
    linkedin: '#'
  },

  {
    name:     'Vitor Barbosa',
    photo:    'vitor-barbosa.jpg',
    github:   'https://github.com/vitorbaraujo',
    gitlab:   'https://gitlab.com/vitorbarbosa',
    linkedin: '#'
  },

  {
    name:     'Renata Soares',
    photo:    'renata.jpg',
    github:   'https://github.com/renatasoares',
    gitlab:   'https://gitlab.com/renatasoares',
    linkedin: '#'
  }
])
